####################################
# Example LAMMPS input script      #
# for a simple Lennard Jones fluid #
####################################

####################################
# 1) Set up simulation box
#   - We set a 3D periodic box
#   - Our box has 10x10x10 atom 
#     positions, evenly distributed
#   - The atom starting sites are
#     separated such that the box density
#     is 0.6
####################################

units		lj
atom_style	atomic
dimension	3
boundary	p p p

lattice		sc 0.60
region		sim_box block 0 40 0 40 0 120
region		atom_start block 1 40 1 40 1 40
create_box	1 sim_box
create_atoms	1 region atom_start

####################################
# 2) Define interparticle interactions
#   - Here, we use truncated & shifted LJ
#   - All atoms of type 1 (in this case, all atoms)
#     have a mass of 1.0
####################################

pair_style	lj/cut 3.5
pair_modify	shift yes
pair_coeff	1 1 1.0 1.0
mass		1 1.0

####################################
# 3) Neighbour lists
#   - Each atom will only consider neighbours
#     within a distance of 2.8 of each other
#   - The neighbour lists are recalculated
#     every timestep
####################################

neighbor        0.3 bin
neigh_modify    delay 10 every 1

####################################
# 4) Define simulation parameters
#   - We fix the temperature and 
#     linear and angular momenta
#     of the system 
#   - We run with fixed number (n),
#     volume (v), temperature (t)
####################################

fix		LinMom all momentum 50 linear 1 1 1 angular
fix		1 all nvt temp 1.00 1.00 5.0

####################################
# 4a) Balance commands
####################################

#fix		shift all balance 10 1.05 shift xyz 10 1.05

#comm_style	tiled
#fix		rcb all balance 10 1.05 rcb

####################################
# 5) Final setup
#   - Define starting particle velocity
#   - Define timestep
#   - Define output system properties (temp, energy, etc.)
#   - Define simulation length
####################################

velocity	all create 1.0 199085 mom no

timestep	0.005

thermo_style	custom step temp etotal pe ke press vol density
thermo		500

dump           2 all custom 1000 positions.lammpstrj id x y z vx vy vz
dump_modify    2 sort id

run_style	verlet

run		10000
