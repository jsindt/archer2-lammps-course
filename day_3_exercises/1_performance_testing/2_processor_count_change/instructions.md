# Using multiple processors to run simulations

So far, all simulations we have run have been run on a single ARCHER node. In this section, we will be looking at what happens when we vary th enumber of nodes assigned to a job. We will be looking at two systems: a small system composed of 1,000 Lennard-Jones particles and a large system composed of 1,046,000 particles. The LAMMPS input files are named `in.1k` and `in.4096k`.

For this exercise, you will need to make changes to the ARCHER queue submission script `lammps_sub.pbs`. To alter the total number of processors being used by LAMMPS, you will need to change the 19<sup>th</sup> line:

```aprun -n 24 lmp_xc30 -in in.1k > output.out```

In the original version of the document, all 24 cores of a single node are used -- you can see this from the `aprun` flag `-n 24`. By varying that number, you can run on a different core count. If you want to run on more than 24 cores, you will need to use more than one nodes. To do this, you must alter the 4th line:

```#PBS -l select=1```

The final number (in this case 1) indicates how many nodes you are wanting to run your system on. **Note** When changing the number of nodes on which you are running, you should also change the number of cores you are using for this job. I strongly recommend that, for jobs using more than one node, the number of cores is given as 24x*N*<sub>nodes</sub>

All of these jobs are launched on the "quick" queue on ARCHER -- this queue allows for short jobs of 20 mins or less to launch quickly. A maximum of 8 nodes can be attributed to any single job, and a single user can have a maximum of one job running and one job queueing at a time.

## Exercise question

Play around with the ARCHER submission script provided and see how increasing and reducing the number of cores used by LAMMPS affects the run time. Try to fill out the table below. Remember to change the LAMMPS input file in the ARCHER submission script to in.4096k to get those results.

*N*<sub>cores</sub> | **1k runtime** | **4096k runtime**
----------------|----------------|----------------
12              |                |
24              |                |
48              |                |
...             |                |
192             |                |